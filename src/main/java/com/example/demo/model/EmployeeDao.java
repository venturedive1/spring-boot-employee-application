package com.example.demo.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class EmployeeDao {
	static List<Employee> list = new ArrayList<>();

	static {
		list.add(new Employee((long) 1234, "Safi", "safi@gmail.com"));
		list.add(new Employee((long) 1235, "Haider", "haider@gmail.com"));
		list.add(new Employee((long) 1236, "Obaid", "obaid@gmail.com"));
	}

	public List<Employee> getAllEmployees() {
		return list;
	}

	public Employee getEmployeeById(Long empId) {
		return list.stream().filter(emp -> emp.getId() == empId).findAny().orElse(null);
	}

	public Employee saveEmployee(Employee emp) {
		emp.setId(list.get(list.size() - 1).getId() + 1);
		list.add(emp);
		return emp;
	}

	public Employee editEmployeeById(Employee newEmployee) {
		Employee employee = list.stream().filter(emp -> emp.getId() == newEmployee.getId()).findAny().orElse(null);
		if (list.indexOf(employee) != -1) {
			list.set(list.indexOf(employee), newEmployee);
			return newEmployee;
		}
		return null;
	}

	public Employee deleteEmployeeById(Long empId) {
		Employee employee = list.stream().filter(emp -> emp.getId() == empId).findAny().orElse(null);
		list.remove(employee);
		return employee;
	}
}
