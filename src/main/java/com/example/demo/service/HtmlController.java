package com.example.demo.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HtmlController {

	@GetMapping("/success")
	public String success() {
		return "success.html";
	}

	@GetMapping("/failure")
	public String failure() {
		return "failure.html";
	}
}
