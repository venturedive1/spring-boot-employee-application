package com.example.demo.service;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.exception_handler.EmployeeNotFound;
import com.example.demo.model.Department;
import com.example.demo.model.DepartmentRepository;
import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeRepository;

@RestController
public class EmployeeRepoController {

	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	DepartmentRepository departmentRepository;

	@GetMapping("/jpa/employees")
	@CrossOrigin
	public List<Employee> getAll() {
		return employeeRepository.findAll();
	}

	@GetMapping("/jpa/employees/{empId}")
	@CrossOrigin
	public EntityModel<Employee> getEmployeeById(@PathVariable Long empId) {
		Employee employee = employeeRepository.findById(empId).get();
		if (employee == null) {
			throw new EmployeeNotFound("Employee Not Found");
		}

		EntityModel<Employee> model = EntityModel.of(employee);
		Link link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getAll())
				.withRel("all-employees");
		model.add(link);
		return model;
	}

	@PostMapping("/jpa/employees")
	@CrossOrigin
	public ResponseEntity<Object> saveEmployee(@Valid @RequestBody Employee emp) {
		Employee employee = employeeRepository.save(emp);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{employeeId}")
				.buildAndExpand(employee.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PostMapping("/jpa/department/{empId}")
	@CrossOrigin
	public ResponseEntity<Object> saveDepartment(@PathVariable Long empId, @RequestBody Department department) {
		Employee employee = employeeRepository.findById(empId).get();

		if (employee == null) {
			throw new EmployeeNotFound("Employee Not Found");
		}

		department.setEmployee(employee);
		departmentRepository.save(department);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{empId}").buildAndExpand(employee.getId())
				.toUri();
		return ResponseEntity.created(uri).build();
	}

	@DeleteMapping("/jpa/employee/{empId}")
	@CrossOrigin
	public void deleteEmployee(@PathVariable Long empId) {
		Employee employee = employeeRepository.findById(empId).get();
		if (employee == null) {
			throw new EmployeeNotFound("Employee Not Found");
		}
		employeeRepository.delete(employee);
	}

}
