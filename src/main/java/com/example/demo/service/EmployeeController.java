package com.example.demo.service;

import com.example.demo.exception_handler.EmployeeNotFound;
import com.example.demo.model.Employee;
import com.example.demo.model.EmployeeDao;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeDao service;

	@GetMapping("/employees")
	@CrossOrigin
	public List<Employee> getAll() {
		return service.getAllEmployees();
	}

	@GetMapping("/employees/{empId}")
	@CrossOrigin
	public EntityModel<Employee> getEmployeeById(@PathVariable Long empId) {
		Employee employee = service.getEmployeeById(empId);
		if (employee == null) {
			throw new EmployeeNotFound("Employee Not Found");
		}

		EntityModel<Employee> model = EntityModel.of(employee);
		Link link = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getAll())
				.withRel("all-employees");
		model.add(link);
		return model;
	}

	@PostMapping("/employees")
	@CrossOrigin
	public ResponseEntity<Object> saveEmployee(@Valid @RequestBody Employee emp) {
		Employee employee = service.saveEmployee(emp);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{employeeId}")
				.buildAndExpand(employee.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PutMapping("/employees")
	@CrossOrigin
	public Employee editEmployee(@RequestBody Employee emp) {
		Employee employee = service.editEmployeeById(emp);
		if (employee == null) {
			throw new EmployeeNotFound("Employee Not Found");
		}
		return employee;
	}

	@DeleteMapping("/employees/{empId}")
	@CrossOrigin
	public Employee deleteEmployee(@PathVariable Long empId) {
		Employee employee = service.deleteEmployeeById(empId);
		if (employee == null) {
			throw new EmployeeNotFound("Employee Not Found");
		}
		return employee;
	}
}
